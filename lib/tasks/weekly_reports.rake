namespace :weekly_reports do

  desc "Print the top 3 active users in every group"
  task active_users_groupwise: :environment do

    score_board = {}.with_indifferent_access
    
    latest_posts = Post.where('created_at>?', Time.now - 7.days).select(:id, :user_id, :group_id)
    score_posts(score_board, latest_posts)

    latest_comments = Comment.where('created_at>?', Time.now - 7.days).preload(:post)
    score_comments(score_board, latest_comments)

    latest_replies = Reply.where('created_at>?', Time.now - 7.days).includes(comment: :post)
    score_replies(score_board, latest_replies)

    print_winners(score_board)
  end

  def score_posts(score_board, posts)
    per_post = 20

    posts.each do |post|
      user_id = "#{post.user_id}"
      group_id = "#{post.group_id}"
      calc_points(score_board, group_id, user_id, per_post)
    end
  end

  def score_comments(score_board, comments)
    per_comment = 5

    comments.each do |comment|
      user_id = "#{comment.user_id}"
      group_id = "#{comment.post.group_id}"
      calc_points(score_board, group_id, user_id, per_comment)
    end
  end

  def score_replies(score_board, replies)
    per_reply = 1

    replies.each do |reply|
      user_id = "#{reply.user_id}"
      group_id = "#{reply.comment.post.group_id}"
      calc_points(score_board, group_id, user_id, per_reply)
    end
  end

  def print_winners(score_board)
    count = 3
    score_board.each do |key, value|
      group = Group.find(key)
      puts ""
      puts "The most active users in the group - #{group.name} are :"
      puts "#{value.sort_by {|_key, value| value}.pop(count).reverse}"
      puts ""
    end
  end

  def calc_points(score_board, group_id, user_id, points)
    if score_board[group_id].nil?
      score_board[group_id] = {
        "#{user_id}": points
      }
    else
      if score_board[group_id][user_id].nil?
        score_board[group_id][user_id] = points
      else
        score_board[group_id][user_id]+=points
      end
    end
  end

end
