namespace :data_migration do

  desc "Data fix task"
  task replies_fix: :environment do

    Comment.where(target_type: Post.name).each do |comment|
      comment.update(post_id: comment.target_id)
    end

    Comment.where(target_type: Comment.name).each do |comment|
      temp = comment
      until temp.target_type.eql?(Post.name)
        temp = temp.target
      end
      comment_id = temp.id
      Reply.create(
        user_id: comment.user_id, comment_id: comment_id,
        content: comment.content, status: comment.status)
    end

    Comment.where(target_type: Comment.name).delete_all
  end

end
