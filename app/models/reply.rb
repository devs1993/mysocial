class Reply < ApplicationRecord

  # Associations
  belongs_to :user
  belongs_to :comment

  # Enums
  enum status: { live: 0, archived: 1, inappropriate: 2 }

end
