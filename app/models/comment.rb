class Comment < ApplicationRecord

  # Associations
  belongs_to :user
  belongs_to :post
  belongs_to :target, polymorphic: true
  has_many :replies

  # Enums
  enum status: { live: 0, archived: 1, inappropriate: 2 }

end
