class Group < ApplicationRecord

  # Associations
  has_many :memberships
  has_many :users, through: :memberships
  has_many :posts

  # Validations
  validates :name, presence: true, uniqueness: true

  # Enums
  enum category: { generic: 0, sports: 1, entertainment: 2, intelect: 3 }
  enum visibility: { closed: 0, open: 1 }
  enum status: { active: 0, archived: 1, suspended: 2 }

end
    