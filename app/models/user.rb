class User < ApplicationRecord

  # Associations
  has_many :memberships
  has_many :groups, through: :memberships
  has_many :posts
  has_many :comments

  # Validations
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :phone_number, presence: true, uniqueness: true

end
