class Membership < ApplicationRecord

  # Associations
  belongs_to :user
  belongs_to :group
  
  # Validations
  validates_uniqueness_of :user_id, scope: :group_id

  # Enums
  enum access_level: { member: 0, admin: 1, owner: 2 }
  enum status: { active: 0, pending_approval: 1, banned: 2, exited: 3 }

end
