class Post < ApplicationRecord

  # Associations
  belongs_to :user
  belongs_to :group
  has_many :comments, as: :target
  has_many :comments
  has_many :sub_comments, through: :comments

  # Enums
  enum status: { draft: 0, live: 1, archived: 2, pending_approval: 3 }

end
