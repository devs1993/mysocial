class CreateMemberships < ActiveRecord::Migration[5.1]
  def change
    create_table :memberships do |t|
      t.references :user, foreign_key: true
      t.references :group, foreign_key: true
      t.integer :access_level
      t.integer :status

      t.timestamps
    end
  end
end
