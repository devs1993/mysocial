class AddValidationsToModels < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :name, :string, null: false
    change_column :users, :email, :string, null: false, unique: true
    change_column :users, :phone_number, :string, null: false, unique: true
    
    change_column :groups, :name, :string, null: false
    change_column :groups, :category, :integer, null: false, default: 0
    change_column :groups, :visibility, :integer, null: false, default: 0
    change_column :groups, :status, :integer, null: false, default: 0
    change_column :groups, :max_count, :integer, null: false, default: 10
    
    change_column :memberships, :user_id, :integer, null: false
    change_column :memberships, :group_id, :integer, null: false
    change_column :memberships, :access_level, :integer, null: false, default: 0
    change_column :memberships, :status, :integer, null: false, default: 0

    change_column :posts, :user_id, :integer, null: false
    change_column :posts, :group_id, :integer, null: false
    change_column :posts, :content, :text, null: false
    change_column :posts, :discussion, :boolean, null: false, default: true
    change_column :posts, :status, :integer, null: false, default: 0

    change_column :comments, :user_id, :integer, null: false
    change_column :comments, :content, :text, null: false
    change_column :comments, :status, :integer, null: false, default: 0
    change_column :comments, :target_type, :string, null: false
    change_column :comments, :target_id, :integer, null: false
  end
end
