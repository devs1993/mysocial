class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.references :user, foreign_key: true
      t.text :content
      t.integer :status
      t.references :target, polymorphic: true, index: true

      t.timestamps
    end
  end
end
