class CreateGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :type
      t.integer :visibility
      t.integer :status
      t.integer :max_count

      t.timestamps
    end
  end
end
