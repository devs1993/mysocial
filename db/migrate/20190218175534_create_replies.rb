class CreateReplies < ActiveRecord::Migration[5.1]
  def change
    create_table :replies do |t|
      t.references :user, foreign_key: true
      t.references :comment, foreign_key: true
      t.text :content
      t.integer :status
      t.text :embedded_url

      t.timestamps
    end
  end
end
